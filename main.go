package main

import (
	"log"
	"net/http"
	"os"

	"github.com/joho/godotenv"
)

func goDotEnvVariable(key string) string {
	err := godotenv.Load(".env")

	if err != nil {
		log.Fatalf("Error loading .env file")
	}

	return os.Getenv((key))
}

func main() {
	port := goDotEnvVariable("PORT")
	if port == "" {
		log.Fatal(" PORT env is required")
	}

	InstanceID := goDotEnvVariable("INSTANCE_ID")

	mux := http.NewServeMux()
	mux.HandleFunc("/", func(rw http.ResponseWriter, r *http.Request) {
		if r.Method != "GET" {
			http.Error(rw, "the requested http method is not allowed", http.StatusMethodNotAllowed)
			return
		}

		text := "hello world"
		if InstanceID != "" {
			text = text + ". from " + InstanceID
		}

		rw.Write([]byte(text))
	})

	server := new(http.Server)
	server.Handler = mux
	server.Addr = ":" + port

	log.Println("web server is starting at ", server.Addr)
	err := server.ListenAndServe()
	if err != nil {
		log.Fatal(err.Error())
	}

}
